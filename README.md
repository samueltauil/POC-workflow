# POC workflow

Once you receive the engagement email, take a note of the opportunity number in Salesforce and follow the workflow.

* Step 1 - [POC Request in SF Email template] (1-poc-request-email.adoc)
* Step 2 - [POC Scope Call Request Email template] (2-initial-email-schedule-scope-call.adoc)
* Step 3 - [POC Prerequisites Email template] (3-prerequisites-email.adoc)
